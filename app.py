# Get instance
import instaloader
import os
from dotenv import load_dotenv
load_dotenv()

USER = os.getenv('USER')
PASSWORD = os.getenv('PASSWORD')

L = instaloader.Instaloader()

# Login or load session
L.login(USER, PASSWORD)        # (login) r''

# Obtain profile metadata
profile = instaloader.Profile.from_username(L.context, "andreavecchietti")

# Print list of followees
follow_list = []
count = 0
# for followee in profile.get_followers():
#     follow_list.append(followee.username)
#     file = open("gente_che_segue_becco.txt", "a+")
#     file.write(follow_list[count])
#     file.write("\n")
#     file.close()
#     print(follow_list[count])
#     count = count+1

# for followee in profile.get_followees():
#     follow_list.append(followee.username)
#     file = open("becco_segue_questi.txt", "a+")
#     file.write(follow_list[count])
#     file.write("\n")
#     file.close()
#     print(follow_list[count])
#     count = count+1

get_followers_count = 0
for followee in profile.get_followers():
    get_followers_count += 1

print(str(get_followers_count)+"\n")

# get_followees_count = 0
# for followee in profile.get_followees():
#     get_followees_count += 1


# print(get_followees_count)
